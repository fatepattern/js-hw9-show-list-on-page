function displayArray(array, parent = document.body){

  let parentElement = document.querySelector(parent);

  let ul = document.createElement("ul");

  for(let i = 0; i < array.length; i++){
    let li = document.createElement("li");
    li.innerHTML = array[i];
    ul.appendChild(li);
  }

  parentElement.appendChild(ul);
}

let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

displayArray(arr, "div");